# Importing Personas

So, [SillyTavern][st] doesn't have the best import for personas. Instead of just storing the backup, I store each persona as their own `markdown` file, with their own images, and then you can just copy/past them into [SillyTavern][st]. I _may_ include a script to build a valid backup `json` file, but I don't see a reason to do that for now. (Only useful for managing large numbers of personas.)

## Markdown Format

The basics of the markdown format are simple. There are the following sections:

* **Name** - the name of the persona
* **Image** - the image for the persona
* **Description** - the description of the persona
* **License** - the license for the persona (and an indication if I own the work or not)

For the most part, just copy the parts you want.

<!-- Links -->

[st]: https://sillytavernai.com/