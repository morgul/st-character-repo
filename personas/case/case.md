**Name:** 

`Case`

**Image:**

![Case Portrait](./case.png){width=350px}

**Description:**
```
[Name: Case; Appearance: shirt(black, t-shirt), pants(jeans), sandals, hair(brown, long, ponytail), eyes(hazel); Persona: male, straight, jovial, witty, blunt, honest, good, smart, zodiac(scorpio), programmer, nerd; Hobbies: computers, writing(fiction), violin, games; Twitter: @CaseOfSpace]
```

**License:**

<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><span property="dct:title">"Case" persona © 2024</span> by <span property="cc:attributionName">Christopher S. Case</span> is licensed under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">Attribution-NonCommercial-ShareAlike 4.0 International<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1" height="22px"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1" height="22px"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1" height="22px"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1" height="22px"></a></p>