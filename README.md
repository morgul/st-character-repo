# SillyTavern Character Repository.

I needed a good place to keep / version my various characters / LoreBooks for [SillyTavern][st]. Also, might use this to publish to [chub.ai][] or [character.ai][] automatically. (Dunno, will investigate.)

## Usage

Typically, you can just grab the json and import it to [SillyTavern][st]. Where there's more steps, I will include instructions of use.

### License

In this repo, I have a mixture of fan-created characters/works and original characters / works. I do not claim ownership of the things I don't own. I will try to denote which things are mine and which are fan creations. If unclear, assume everything is <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-NC-SA 4.0</a>.

**tl;dr**: You can remix/reuse for non-commercial works, but if you publish anywhere, credit me. (You can credit my username 'Morgul', or my actual name 'Christopher S. Case'.)

---

<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><span property="dct:title">All original works</span> by <span property="cc:attributionName">Christopher S. Case</span> is licensed under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-NC-SA 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1" height="22px"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1" height="22px"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1" height="22px"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1" height="22px"></a></p>

<!-- Links -->

[st]: https://sillytavernai.com/
[chub.ai]: https://chub.ai/
[character.ai]: https://beta.character.ai/