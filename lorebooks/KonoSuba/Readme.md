# KonoSuba

These are lorebooks from the [KonoSuba][] anime setting. As I've only seen the anime, and I heavily use the [wiki][], its entirely possible that elements of the anime and the manga have been combined. The intention is to have a lorebooks appropriate for the beginning of the story.

## General Lorebook

This lorebook has been heavily modifies/adapted from https://chub.ai/lorebooks/ITzGab/konosuba-63e0ee92. Converted to PLists and expanded/corrected. Made more generic and corrected to be beginning of the story appropriate.

The intention is for this to be a generally active Lorebook for any KonoSuba chats, while the individual lorebooks are to be used as the character lorebooks for each character.

## Character Lorebooks

These are named for each of the characters, and the intention is for these to be the character lorebooks, with the generic lorebook being added as a secondary world lorebook.

<!-- Links -->

[wiki]: https://konosuba.fandom.com/wiki/Kono_Subarashii_Sekai_ni_Shukufuku_wo!_Wiki
[KonoSuba]: https://en.wikipedia.org/wiki/KonoSuba