# Lorebooks

Here I have collected various lorebooks I've written or heavily modified. These lorebooks are as optimized as I can make them, opting for the best format/memory performance I can get. as such, all of my lorebook writing comes from here:

* https://rentry.co/world-info-encyclopedia

This means, the lore books are designed with the PLists / Ali:Chat format in mind, as its the most token-rich, reliable way for the characters to function.

The basic concept behind how the lorebooks are structured is this:

* World - Simple PList
* Character - Combination of PList + Ali:Chat

When the character is reacting to the a World entry, just an Ali:Chat entry is needed. When the character is reacting to a something personal, there should be a PList entry and a (non-recursive) Ali:Chat entry.

## Work in Progress

Not all lorebooks are converted over to PList style, nor are they necessarily _good_. I'm still learning and experimenting. They should improve over time.